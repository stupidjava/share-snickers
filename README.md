# Share Snickers

This is a repo for SQR course project.

### Project description:

• In general: platform where people can share their piece of snickers

• 2 options: i wanna eat & i wanna share

• The service will find the closest person which whill give or take your snickers

• thats it, simplestupid problems, simplestupid solutions

### Technology stack

Java - for backend (server with db)

Spring - for Web application (cient views)

### Requirements

Java 11

Gradle 6.7.1

Spring 2.3.3

PostgreSQL 12.3

### Branching policy
- Adding new feature
`feature/featurename`
- Fixing
`fix/featurename`
- Adding tests
`testing/unit`
