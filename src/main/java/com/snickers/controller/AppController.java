package com.snickers.controller;

import com.snickers.dto.MatchApproveDTO;
import com.snickers.dto.PlaceActionDTO;
import com.snickers.dto.ReplyToActionDTO;
import com.snickers.model.Action;
import com.snickers.service.ActionService;
import com.snickers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AppController {

    @Autowired
    private ActionService actionService;

    @Autowired
    private UserService userService;

    @PostMapping("/placeAction")
    @ResponseBody
    public Action placeAction(
            @RequestBody PlaceActionDTO placeActionDTO) {
        return actionService.createPlaceAction(placeActionDTO);
    }

    @PostMapping("/replyToAction")
    public Action replyToAction(
            @RequestBody ReplyToActionDTO replyToActionDTO) {
        return actionService.updateActionWithReply(replyToActionDTO);
    }

    @PostMapping("/matchApprove")
    public Action matchApprove(
    @RequestBody MatchApproveDTO matchApproveDTO) {
        return actionService.updateActionWithApprove(matchApproveDTO);
    }

    @GetMapping("/getAllAvailableActions")
    @ResponseBody
    public List<Action> getAllAvailableActions(){
        return  actionService.getAllAvailableActions();
    }

    @GetMapping("/getAllUserClosedActions")
    @ResponseBody
    public List<Action> getAllUserClosedActions(Authentication authentication){
        var user = userService.getUserByUsername(authentication.getName());
        return  actionService.getUserClosedActions(user.getId());
    }
    @GetMapping("/getAllUserOpenActions")
    @ResponseBody
    public List<Action> getAllUserOpenActions(Authentication authentication){
        var user = userService.getUserByUsername(authentication.getName());
        return  actionService.getUserOpenActions(user.getId());
    }
}
