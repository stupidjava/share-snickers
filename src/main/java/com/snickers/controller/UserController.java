package com.snickers.controller;

import com.snickers.dto.RegistrationDTO;
import com.snickers.dto.UserInfoDTO;
import com.snickers.model.User;
import com.snickers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getUserInfo")
    @ResponseBody
    public UserInfoDTO getUserInfo(@RequestParam(value = "user_id") Long userId) {

        return userService.getUserInfo(userId);
    }

    @GetMapping("/getCurrentUser")
    @PreAuthorize("isAuthenticated()")
    public UserInfoDTO getCurrentUser(Principal principal) {
        return userService.getUserInfo(principal.getName());
    }

    @PostMapping("/auth/register")
    public String register(@ModelAttribute("data") RegistrationDTO registration){
        var user = new User();
        user.setAdmin(false);
        user.setEmail(registration.getEmail());
        user.setName(registration.getName());
        user.setFavouriteChoc(0);
        user.setPhoneNumber(registration.getPhoneNumber());
        user.setCity(registration.getCity());
        user.setReceivedAmount(0);
        user.setSharedAmount(0);
        user = userService.createUser(user);
        userService.setUserPassword(user.getEmail(),registration.getPassword());
        return "redirect:/home";
    }

}
