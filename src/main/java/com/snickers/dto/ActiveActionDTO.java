package com.snickers.dto;

public class ActiveActionDTO {

    private Long actionID;
    private String comment;
    private String otherName;
    private String otherLocation;
    private String otherPhone;
    private boolean isReceiver;
    private boolean hasAcceptor;

    public ActiveActionDTO(){}

    public ActiveActionDTO(Long actionID, String comment, String otherName, String otherLocation, String otherPhone, boolean isReceiver, boolean hasAcceptor) {
        this.actionID = actionID;
        this.comment = comment;
        this.otherName = otherName;
        this.otherLocation = otherLocation;
        this.otherPhone = otherPhone;
        this.isReceiver = isReceiver;
        this.hasAcceptor = hasAcceptor;
    }

    public Long getActionID() {
        return actionID;
    }

    public void setActionID(Long actionID) {
        this.actionID = actionID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getOtherLocation() {
        return otherLocation;
    }

    public void setOtherLocation(String otherLocation) {
        this.otherLocation = otherLocation;
    }

    public String getOtherPhone() {
        return otherPhone;
    }

    public void setOtherPhone(String otherPhone) {
        this.otherPhone = otherPhone;
    }

    public boolean isReceiver() {
        return isReceiver;
    }

    public void setReceiver(boolean receiver) {
        isReceiver = receiver;
    }

    public boolean isHasAcceptor() {
        return hasAcceptor;
    }

    public void setHasAcceptor(boolean hasAcceptor) {
        this.hasAcceptor = hasAcceptor;
    }
}
