package com.snickers.dto;

import com.snickers.model.Action;

public class AvailableActionDTO {

    private Long actionID;
    private Action.Type type;
    private Long initiatorId;
    private Long chocTypeId;
    private String comment;
    private String initiatorName;
    private String initiatorLocation;
    private String initiatorPhone;

    public AvailableActionDTO(Long actionID, Action.Type type, Long initiatorId, Long chocTypeId, String comment, String initiatorName, String initiatorLocation, String initiatorPhone) {
        this.actionID = actionID;
        this.type = type;
        this.initiatorId = initiatorId;
        this.chocTypeId = chocTypeId;
        this.comment = comment;
        this.initiatorName = initiatorName;
        this.initiatorLocation = initiatorLocation;
        this.initiatorPhone = initiatorPhone;
    }

    public AvailableActionDTO()
    {
        actionID = 0L;
        type = Action.Type.OFFER_PUBLIC;
        initiatorId = 0L;
        chocTypeId = 0L;
        comment = "";
        initiatorName = "";
        initiatorLocation = "";
        initiatorPhone = "";
    }

    public Long getActionID() {
        return actionID;
    }

    public void setActionID(Long actionID) {
        this.actionID = actionID;
    }

    public Action.Type getType() {
        return type;
    }

    public void setType(Action.Type type) {
        this.type = type;
    }

    public Long getInitiatorId() {
        return initiatorId;
    }

    public void setInitiatorId(Long initiatorId) {
        this.initiatorId = initiatorId;
    }

    public Long getChocTypeId() {
        return chocTypeId;
    }

    public void setChocTypeId(Long chocTypeId) {
        this.chocTypeId = chocTypeId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getInitiatorName() {
        return initiatorName;
    }

    public void setInitiatorName(String initiatorName) {
        this.initiatorName = initiatorName;
    }

    public String getInitiatorLocation() {
        return initiatorLocation;
    }

    public void setInitiatorLocation(String initiatorLocation) {
        this.initiatorLocation = initiatorLocation;
    }

    public String getInitiatorPhone() {
        return initiatorPhone;
    }

    public void setInitiatorPhone(String initiatorPhone) {
        this.initiatorPhone = initiatorPhone;
    }

}
