package com.snickers.dto;

import com.snickers.model.Action;

public class MatchApproveDTO {
    private long actionId;
    private Action.Status status;

    public MatchApproveDTO() {
    }

    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    public Action.Status getStatus() {
        return status;
    }

    public void setStatus(Action.Status status) {
        this.status = status;
    }

    public MatchApproveDTO(long actionId, Action.Status status) {
        this.actionId = actionId;
        this.status = status;
    }
}
