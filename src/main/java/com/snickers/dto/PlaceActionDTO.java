package com.snickers.dto;

import com.snickers.model.Action;

public class PlaceActionDTO {

    private Action.Type type;
    private Long initiatorId;
    private Long chocTypeId;
    private int chocWeight;
    private String comment;

    public PlaceActionDTO(Action.Type type, Long initiatorId, Long chocTypeId, int chocWeight, String comment) {
        this.type = type;
        this.initiatorId = initiatorId;
        this.chocTypeId = chocTypeId;
        this.chocWeight = chocWeight;
        this.comment = comment;
    }

    public void setType(Action.Type type) {
        this.type = type;
    }

    public void setInitiatorId(Long initiatorId) {
        this.initiatorId = initiatorId;
    }

    public void setChocTypeId(Long chocTypeId) {
        this.chocTypeId = chocTypeId;
    }

    public void setChocWeight(int chocWeight) {
        this.chocWeight = chocWeight;
    }

    public PlaceActionDTO() {
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Action.Type getType() {
        return type;
    }

    public Long getInitiatorId() {
        return initiatorId;
    }

    public Long getChocTypeId() {
        return chocTypeId;
    }

    public int getChocWeight() {
        return chocWeight;
    }

    public String getComment() {
        return comment;
    }
}
