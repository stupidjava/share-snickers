package com.snickers.dto;

import com.snickers.model.Action;

public class ReplyToActionDTO {
    private long actionId;
    private Action.Status status;
    private long acceptorId;

    public ReplyToActionDTO(long actionId, Action.Status status, long acceptorId) {
        this.actionId = actionId;
        this.status = status;
        this.acceptorId = acceptorId;
    }

    public ReplyToActionDTO() {
    }

    public long getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public Action.Status getStatus() {
        return status;
    }

    public void setStatus(Action.Status status) {
        this.status = status;
    }

    public long getAcceptorId() {
        return acceptorId;
    }

    public void setAcceptorId(int acceptorId) {
        this.acceptorId = acceptorId;
    }
}
