package com.snickers.dto;

public class UserInfoDTO {
    private Long userId;
    private String username;
    private String phoneNumber;
    private String email;
    private String city;
    private int sharedAmount;
    private int receivedAmount;
    private int favoriteChoc;

    public UserInfoDTO() {

    }

    public UserInfoDTO(Long userId, String username, String phoneNumber, String email, String city, int sharedAmount,
                       int receivedAmount, int favoriteChoc) {
        this.userId = userId;
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.city = city;
        this.sharedAmount = sharedAmount;
        this.receivedAmount = receivedAmount;
        this.favoriteChoc = favoriteChoc;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setSharedAmount(int sharedAmount) {
        this.sharedAmount = sharedAmount;
    }

    public void setReceivedAmount(int receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public void setFavoriteChoc(int favoriteChoc) {
        this.favoriteChoc = favoriteChoc;
    }

    public Long getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public int getSharedAmount() {
        return sharedAmount;
    }

    public int getReceivedAmount() {
        return receivedAmount;
    }

    public int getFavoriteChoc() {
        return favoriteChoc;
    }
}
