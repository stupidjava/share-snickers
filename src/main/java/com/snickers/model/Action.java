package com.snickers.model;

import javax.persistence.*;

@Entity
@Table(name = "actions")
public class Action {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "type")
    private Type type;

    @Column(name = "status")
    private Status status;

    @Column(name = "initiator_id", nullable = false)
    private Long initiatorId;

    @Column(name = "acceptor_id", nullable = true)
    private Long acceptorId;

    @Column(name = "choc_type_id", nullable = false)
    private Long chocType;

    @Column(name = "choc_weight")
    private Integer chocWeight;

    @Column(name = "comment")
    private String comment;

    public Action(Type type, Status status, Long initiator, Long acceptor, Long chocType, Integer chocWeight, String comment) {
        this.type = type;
        this.status = status;
        this.initiatorId = initiator;
        this.acceptorId = acceptor;
        this.chocType = chocType;
        this.chocWeight = chocWeight;
        this.comment = comment;
    }

    public Action() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getInitiator() {
        return initiatorId;
    }

    public void setInitiator(Long initiator) {
        this.initiatorId = initiator;
    }

    public Long getAcceptor() {
        return acceptorId;
    }

    public void setAcceptor(Long acceptor) {
        this.acceptorId = acceptor;
    }

    public Long getChocType() {
        return chocType;
    }

    public void setChocType(Long chocType) {
        this.chocType = chocType;
    }

    public Integer getChocWeight() {
        return chocWeight;
    }

    public void setChocWeight(Integer chocWeight) {
        this.chocWeight = chocWeight;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public enum Type {
        REQUEST,
        OFFER_PUBLIC,
        OFFER_PERSONAL,
    }

    public enum Status {
        OPEN,
        CANCELED,
        CLOSED,
    }

}
