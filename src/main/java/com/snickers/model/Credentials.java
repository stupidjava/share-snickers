package com.snickers.model;

import javax.persistence.*;

@Entity
@Table(name = "credentials")
public class Credentials {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "cred_id_gen_seq")
    @SequenceGenerator(name = "cred_id_gen_seq", initialValue = 1000)
    Long id;

    @Column(name = "user_id")
    Long userId;

    @Column(name = "password")
    String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
