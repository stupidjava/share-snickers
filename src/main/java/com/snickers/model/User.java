package com.snickers.model;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "user_id_gen_seq")
    @SequenceGenerator(name = "user_id_gen_seq", initialValue = 1000)
    private Long id;

    @Column(name = "is_admin")
    private Boolean isAdmin;

    @Column(name = "name")
    private String name;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "city")
    private String city;

    @Column(name = "shared_amount")
    private Integer sharedAmount;

    @Column(name = "received_amount")
    private Integer receivedAmount;

    @Column(name = "favourite_choc")
    private Integer favouriteChoc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        this.isAdmin = admin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getSharedAmount() {
        return sharedAmount;
    }

    public void setSharedAmount(Integer sharedAmount) {
        this.sharedAmount = sharedAmount;
    }

    public Integer getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(Integer receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public Integer getFavouriteChoc() {
        return favouriteChoc;
    }

    public void setFavouriteChoc(Integer favouriteChoc) {
        this.favouriteChoc = favouriteChoc;
    }
}
