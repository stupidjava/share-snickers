package com.snickers.repository;

import com.snickers.model.Action;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActionRepository extends CrudRepository<Action, Long> {
    List<Action> findActionsByAcceptorIdIsNull();
    List<Action> findActionsByAcceptorIdIsNullAndTypeEquals(Action.Type t);

    List<Action> findActionsByInitiatorIdEqualsAndStatusEqualsOrAcceptorIdEqualsAndStatusEquals(Long iId,  Action.Status s1,Long aId, Action.Status s2);
}
