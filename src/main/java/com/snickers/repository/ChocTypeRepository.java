package com.snickers.repository;

import com.snickers.model.ChocType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChocTypeRepository extends CrudRepository<ChocType, Long> {
}
