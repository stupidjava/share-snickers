package com.snickers.repository;

import com.snickers.model.Credentials;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CredentialsRepository extends CrudRepository<Credentials,Long> {
    Optional<Credentials> findCredentialsByUserIdEquals(Long userId);


}
