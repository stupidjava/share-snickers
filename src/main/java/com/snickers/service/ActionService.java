package com.snickers.service;

import com.snickers.dto.MatchApproveDTO;
import com.snickers.dto.PlaceActionDTO;
import com.snickers.dto.ReplyToActionDTO;
import com.snickers.model.Action;
import com.snickers.model.User;
import com.snickers.repository.ActionRepository;
import com.snickers.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActionService {
    private final ActionRepository actionRepository;

    private final UserRepository userRepository;

    public ActionService(@Autowired ActionRepository actionRepository, @Autowired UserRepository userRepository) {
        this.actionRepository = actionRepository;
        this.userRepository = userRepository;
    }

    public Action createAction(Action action) {

        return actionRepository.save(action);
    }

    public Action createPlaceAction(PlaceActionDTO placeActionDTO) {
        Action action = new Action(placeActionDTO.getType(), Action.Status.OPEN, placeActionDTO.getInitiatorId()
                , null, placeActionDTO.getChocTypeId(), placeActionDTO.getChocWeight(),
                placeActionDTO.getComment());

        actionRepository.save(action);

        return action;
    }

    public Action updateActionWithReply(ReplyToActionDTO replyToActionDTO) {
        Action action = actionRepository.findById(replyToActionDTO.getActionId()).orElse(null);
        if (action == null) {
            return null;
        }
        action.setStatus(replyToActionDTO.getStatus());
        action.setAcceptor(replyToActionDTO.getAcceptorId());
        return actionRepository.save(action);

    }

    public Action updateAction(Action action) {
        return actionRepository.save(action);
    }

    @Nullable
    public Action getActionById(long actionId) {
        return actionRepository.findById(actionId).orElse(null);
    }

    public Action updateActionWithApprove(MatchApproveDTO matchApproveDTO) {
        Action action = actionRepository.findById(matchApproveDTO.getActionId()).orElse(null);

        if (action == null) {
            return null;
        }

        boolean requestAction = action.getType() == Action.Type.REQUEST;

        User sharer;
        User receiver;
        long id1 = requestAction ? action.getAcceptor() : action.getInitiator();
        long id2 = requestAction ? action.getInitiator() : action.getAcceptor();
        if (userRepository.findById(id1).isPresent()
                && userRepository.findById(id2).isPresent()) {
            sharer = userRepository.findById(id1).get();
            receiver = userRepository.findById(id2).get();
        }
        else {
            return null;
        }

        sharer.setSharedAmount(sharer.getSharedAmount() + 1);
        receiver.setReceivedAmount(receiver.getReceivedAmount() + 1);

        userRepository.save(sharer);
        userRepository.save(receiver);

        action.setStatus(matchApproveDTO.getStatus());
        return actionRepository.save(action);
    }

    public List<Action> getAllAvailableActions(){
        return actionRepository.findActionsByAcceptorIdIsNull();
    }

    public List<Action> getAllShareActions(){
        return actionRepository.findActionsByAcceptorIdIsNullAndTypeEquals(Action.Type.OFFER_PUBLIC);
    }

    public List<Action> getAllReceiveActions(){
        return actionRepository.findActionsByAcceptorIdIsNullAndTypeEquals(Action.Type.REQUEST);
    }

    public List<Action> getUserClosedActions(long userId){
        return actionRepository.findActionsByInitiatorIdEqualsAndStatusEqualsOrAcceptorIdEqualsAndStatusEquals(userId,Action.Status.CLOSED, userId,Action.Status.CLOSED);
    }

    public List<Action> getUserOpenActions(long userId){
        return  actionRepository.findActionsByInitiatorIdEqualsAndStatusEqualsOrAcceptorIdEqualsAndStatusEquals(userId,Action.Status.OPEN, userId, Action.Status.OPEN);
    }
}
