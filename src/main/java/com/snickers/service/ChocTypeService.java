package com.snickers.service;

import com.snickers.model.ChocType;
import com.snickers.repository.ChocTypeRepository;
import com.sun.istack.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChocTypeService {
    final private ChocTypeRepository chocTypeRepository;

    public ChocTypeService(@Autowired ChocTypeRepository chocTypeRepository) {
        this.chocTypeRepository = chocTypeRepository;
    }

    public ChocType createChocType(ChocType chocType) {
        return chocTypeRepository.save(chocType);
    }

    public ChocType updateAction(ChocType chocType) {
        return chocTypeRepository.save(chocType);
    }

    @Nullable
    public ChocType getChocTypeById(long chocTypeId) {
        return chocTypeRepository.findById(chocTypeId).orElse(null);
    }

}
