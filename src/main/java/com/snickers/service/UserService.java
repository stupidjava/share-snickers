package com.snickers.service;

import com.snickers.dto.UserInfoDTO;
import com.snickers.model.Credentials;
import com.snickers.model.User;
import com.snickers.repository.CredentialsRepository;
import com.snickers.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final CredentialsRepository credentialsRepository;

    public UserService(@Autowired UserRepository userRepository, @Autowired CredentialsRepository credentialsRepository) {
        this.userRepository = userRepository;
        this.credentialsRepository = credentialsRepository;
    }

    public User createUser(User user) {
        return userRepository.save(user);
    }

    public User updateUser(User user) {
        return userRepository.save(user);
    }

    public User getUserByUsername(String name) {
        return userRepository.findUserByEmail(name).orElse(null);
    }

    @Nullable
    public User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    public List<User> findAll() {

        return (List<User>) userRepository.findAll();
    }

    public UserInfoDTO getUserInfo(Long id) {
        User user = userRepository.findById(id).orElse(null);
        return ConvertUserToUserInfoDTO(user);
    }

    public UserInfoDTO getUserInfo(String email) {
        User user = userRepository.findUserByEmail(email).orElse(null);
        return ConvertUserToUserInfoDTO(user);
    }

    public boolean setUserPassword(String email,String newPassword) {
        var user = userRepository.findUserByEmail(email);
        if (user.isPresent()) {
            var credentials = credentialsRepository.findCredentialsByUserIdEquals(user.get().getId());
            Credentials cred;
            if (credentials.isPresent()) {
                cred = credentials.get();
            } else {
                cred = new Credentials();
                cred.setUserId(user.get().getId());
            }
            cred.setPassword(newPassword);
            credentialsRepository.save(cred);
            return true;
        } else {
            return false;
        }
    }

    public List<UserInfoDTO> getUserRating(boolean sortByShared) {
        final int ratingPosNum = 10;

        ArrayList<UserInfoDTO> rating = new ArrayList<>(ratingPosNum);

        for (int i = 0; i < ratingPosNum; i++) {
            rating.add(null);
        }

        for (User user : userRepository.findAll()) {
            for (int i = 0; i < ratingPosNum; i++) {
                if (rating.get(i) == null) {
                    rating.set(i, ConvertUserToUserInfoDTO(user));
                    break;
                }
                if (sortByShared) {
                    if (user.getSharedAmount() > rating.get(i).getSharedAmount()) {
                        rating.add(i, ConvertUserToUserInfoDTO(user));
                        rating.remove(rating.size() - 1);
                        break;
                    }
                } else {
                    if (user.getReceivedAmount() > rating.get(i).getReceivedAmount()) {
                        rating.add(i, ConvertUserToUserInfoDTO(user));
                        rating.remove(rating.size() - 1);
                        break;
                    }
                }
            }
        }

        rating.removeIf(Objects::isNull);

        return rating;
    }

    private UserInfoDTO ConvertUserToUserInfoDTO(User user) {
        if (user == null) {
            return null;
        }
        return new UserInfoDTO(user.getId(), user.getName(), user.getPhoneNumber(), user.getEmail(), user.getCity(),
                user.getSharedAmount(), user.getReceivedAmount(), user.getFavouriteChoc());
    }
}
