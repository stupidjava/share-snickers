package com.snickers.web;

import com.snickers.dto.*;
import com.snickers.model.Action;
import com.snickers.model.User;
import com.snickers.service.ActionService;
import com.snickers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Controller
public class IndexViewController {

    @Autowired
    private ActionService actionService;

    @Autowired
    private UserService userService;

    private final String redirect = "redirect:/";

    @GetMapping("/")
    public String getIndex(Model model, Principal principal) {

        long userID = userService.getUserByUsername(principal.getName()).getId(); // todo take user from auth

        model.addAttribute("shareOffers", getAvailableActions(true, userID));
        model.addAttribute("receiveOffers", getAvailableActions(false, userID));
        model.addAttribute("activeOffers", getActiveActions(userID));

        return "index";
    }

    public List<ActiveActionDTO> getActiveActions(long userID)
    {
        ArrayList<ActiveActionDTO> result = new ArrayList<>();

        for (var availableAction:
             actionService.getUserOpenActions(userID)) {

            ActiveActionDTO dto = new ActiveActionDTO();

            dto.setReceiver( ((availableAction.getInitiator() == userID) && availableAction.getType() == Action.Type.REQUEST ) ||
                    (availableAction.getType() == Action.Type.OFFER_PUBLIC && (availableAction.getInitiator() != userID)) );
            dto.setActionID( availableAction.getId());
            dto.setHasAcceptor(true);
            if(availableAction.getAcceptor() == null)
            {
                dto.setComment("Waiting for acceptor");
                dto.setHasAcceptor(false);

                dto.setOtherName(dto.isReceiver() ? "Guy with snickers" : "Guy without snickers");
            } else if(availableAction.getInitiator() == userID)
            {
                User user = userService.getUserById(availableAction.getAcceptor());
                if(user == null) {
                    continue;
                }
                dto.setOtherName(user.getName());
                dto.setOtherLocation(user.getCity());
                dto.setOtherPhone( user.getPhoneNumber());
                if(dto.isReceiver())
                {
                    dto.setComment("Receive the item and approve on the web");
                }else{
                    dto.setComment("Share the item and wait for approve");
                }
            } else {
                User user = userService.getUserById(availableAction.getInitiator());
                if (user == null) {
                    continue;
                }
                dto.setOtherName(user.getName());
                dto.setOtherLocation(user.getCity());
                dto.setOtherPhone( user.getPhoneNumber());
                if(dto.isReceiver())
                {
                    dto.setComment("Receive the item and approve on the web");
                }else{
                    dto.setComment( "Share the item and wait for approve");
                }
            }

            result.add(dto);
        }

        return  result;
    }

    public List<AvailableActionDTO> getAvailableActions(boolean isShare, long userID)
    {
        List<Action> shareOffers ;
        if (isShare) {
            shareOffers = actionService.getAllShareActions();
        } else {
            shareOffers = actionService.getAllReceiveActions();
        }

        List<Action> temp =  new ArrayList<Action>(); // poshel govnokod
        int count = 0;
        int maxOffers = 10;
        for (var offer:
             shareOffers) {
            if(offer.getInitiator() == userID){
                continue;
            }

            temp.add(offer);
            count++;
            if(count > maxOffers){
                break;
            }
        }
        shareOffers = temp;

        LinkedList<AvailableActionDTO> shareOffersDTOList = new LinkedList<>();
        for (var availableAction:
                shareOffers) {

            AvailableActionDTO dto = new AvailableActionDTO();
            User user = userService.getUserById(availableAction.getInitiator());
            dto.setInitiatorName("Searching...");
            if(user != null)
            {
                dto.setInitiatorName(user.getName());
                dto.setInitiatorPhone(user.getPhoneNumber());
                dto.setInitiatorLocation(user.getCity());
            }

            dto.setActionID(availableAction.getId());
            dto.setComment(availableAction.getComment());
            dto.setChocTypeId(availableAction.getChocType());
            dto.setType(availableAction.getType());
            dto.setInitiatorId( availableAction.getInitiator());
            shareOffersDTOList.add(dto);
        }
        return  shareOffersDTOList;
    }

    @GetMapping("/newAction")
    public String newAction(@RequestParam(value = "actionType") Integer type, @RequestParam(value = "comment") String comment, Principal principal)
    {
        Long userID = userService.getUserByUsername(principal.getName()).getId();

        PlaceActionDTO placeActionDTO = new PlaceActionDTO();
        if(type == 0)
        {
            placeActionDTO.setType(Action.Type.REQUEST);
        }else{
            placeActionDTO.setType(Action.Type.OFFER_PUBLIC);
        }

        placeActionDTO.setChocTypeId(0L);
        placeActionDTO.setComment(comment);
        placeActionDTO.setInitiatorId(userID);
        placeActionDTO.setChocWeight(0);
        actionService.createPlaceAction(placeActionDTO);
        return redirect;
    }

    @GetMapping("/accept")
    public String acceptAction(@RequestParam(value = "actionID") Integer actionId, Principal principal)
    {
        Long userID = userService.getUserByUsername(principal.getName()).getId();

        ReplyToActionDTO replyToActionDTO = new ReplyToActionDTO(actionId,Action.Status.OPEN ,userID);

        actionService.updateActionWithReply(replyToActionDTO);
        return redirect;
    }

    @GetMapping("/cancelAction")
    public String cancelAction(@RequestParam(value = "actionID") Integer actionId, Principal principal)
    {
        Long userID = userService.getUserByUsername(principal.getName()).getId();

        ReplyToActionDTO replyToActionDTO = new ReplyToActionDTO(actionId,Action.Status.CANCELED ,userID);

        actionService.updateActionWithReply(replyToActionDTO);
        return redirect;
    }

    @GetMapping("/approveAction")
    public String approveAction(@RequestParam(value = "actionID") Integer actionId)
    {
        MatchApproveDTO replyToActionDTO = new MatchApproveDTO(actionId, Action.Status.CLOSED);

        actionService.updateActionWithApprove(replyToActionDTO);
        return redirect;
    }

    @GetMapping("/home")
    public String getHome(Model model) {
        return redirect;
    }
}
