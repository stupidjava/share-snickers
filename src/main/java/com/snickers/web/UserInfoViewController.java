package com.snickers.web;

import com.snickers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class UserInfoViewController {
    @Autowired
    private UserService userService;

    @GetMapping("/user")
    public String user(Model model, Principal principal) {
        var user = userService.getUserByUsername(principal.getName());
        model.addAttribute("user", user);
        return "user";
    }

    @GetMapping("/user/updateData")
    public String updateData(@RequestParam(value = "name") String name, @RequestParam(value = "city") String city, @RequestParam(value = "phone_number") String phoneNumber, Principal principal)
    {
        var user = userService.getUserByUsername(principal.getName());
        user.setCity(city);
        user.setName(name);
        user.setPhoneNumber(phoneNumber);

        userService.updateUser(user);
        return "redirect:/user";
    }
}
