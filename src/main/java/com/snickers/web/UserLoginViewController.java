package com.snickers.web;

import com.snickers.dto.RegistrationDTO;
import com.snickers.model.User;
import com.snickers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class UserLoginViewController {

    @Autowired
    UserService userService;

    //@RequestMapping("/auth/login")
    public String login(Model model) {
        model.addAttribute("aaa","bbb");
        return "auth/login";
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("data",new RegistrationDTO());
        return "auth/register";
    }

    @GetMapping("/afterOauth2")
    public String googlePreRegister(Model model, Principal principal){

        if(userService.getUserByUsername(principal.getName()) == null)
        {
            User user = new User();
            user.setName(principal.getName());
            user.setEmail(principal.getName());
            user.setCity("Dark forest");
            user.setFavouriteChoc(0);
            user.setPhoneNumber("+799912345678");
            user.setSharedAmount(0);
            user.setReceivedAmount(0);
            userService.createUser(user);
        }

        return "redirect:/";
    }
}
