package com.snickers.web;

import com.snickers.dto.UserInfoDTO;
import com.snickers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class UserRatingViewController {

    @Autowired
    private UserService userService;

    @GetMapping("/leaderboards")
    public String leaderboards(Model model) {

        List<UserInfoDTO> sharedList = userService.getUserRating(true);
        List<UserInfoDTO> receivedList = userService.getUserRating(false);

        List<Pair<Integer, UserInfoDTO>> sharedRating = new ArrayList<>();
        List<Pair<Integer, UserInfoDTO>> receivedRating = new ArrayList<>();
        for (int i = 0; i < sharedList.size(); i++) {
            sharedRating.add(Pair.of(i + 1, sharedList.get(i)));
            receivedRating.add(Pair.of(i + 1, receivedList.get(i)));
        }

        model.addAttribute("shared_users", sharedRating);
        model.addAttribute("received_users", receivedRating);

        return "leaderboards";
    }

}
