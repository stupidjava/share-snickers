package com.snickers.config;

import com.snickers.controller.AppController;
import com.snickers.controller.UserController;
import com.snickers.repository.ActionRepository;
import com.snickers.repository.ChocTypeRepository;
import com.snickers.repository.CredentialsRepository;
import com.snickers.repository.UserRepository;
import com.snickers.service.ActionService;
import com.snickers.service.UserService;
import com.snickers.web.IndexViewController;
import com.snickers.web.UserInfoViewController;
import com.snickers.web.UserRatingViewController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;

@Configuration
public class TestConfig {

    @Bean
    public CredentialsRepository credentialsRepository(){return  mock(CredentialsRepository.class);}

    @Bean
    public UserService userService() {
        return mock(UserService.class);
    }

    @Bean
    public ActionService actionService() {
        return mock(ActionService.class);
    }

    @Bean
    public UserRepository userRepository() {
        return mock(UserRepository.class);
    }

    @Bean
    public ActionRepository actionRepository() {
        return mock(ActionRepository.class);
    }

    @Bean
    public ChocTypeRepository chocTypeRepository() {
        return mock(ChocTypeRepository.class);
    }

    @Bean
    public AppController appController() {
        return new AppController();
    }

    @Bean
    public UserController userController() {
        return new UserController();
    }

    @Bean
    public IndexViewController indexViewController() {
        return new IndexViewController();
    }

    @Bean
    public UserInfoViewController userInfoViewController() {
        return new UserInfoViewController();
    }

    @Bean
    public UserRatingViewController userRatingViewController() {
        return new UserRatingViewController();
    }

}
