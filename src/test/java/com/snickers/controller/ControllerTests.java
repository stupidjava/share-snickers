package com.snickers.controller;

import com.snickers.config.TestConfig;
import com.snickers.dto.*;
import com.snickers.model.Action;
import com.snickers.model.User;
import com.snickers.service.ActionService;
import com.snickers.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
public class ControllerTests {

    @Autowired
    private AppController appController;

    @Autowired
    private UserController userController;

    @Autowired
    private ActionService actionService;

    @Autowired
    private UserService userService;

    private PlaceActionDTO placeActionDTO = mock(PlaceActionDTO.class);
    private ReplyToActionDTO replyToActionDTO = mock(ReplyToActionDTO.class);
    private MatchApproveDTO matchApproveDTO = mock(MatchApproveDTO.class);
    private Action action = mock(Action.class);

    private User u1;

    @BeforeEach
    void TestSetup() {
        u1 = new User();
        u1.setId(220L);
        u1.setName("name_1");
        u1.setReceivedAmount(5);
        u1.setSharedAmount(1);
        u1.setFavouriteChoc(1);
    }

    @Test
    void PlaceActionTest() {
        when(actionService.createPlaceAction(placeActionDTO)).thenReturn(action);
        Action actual = appController.placeAction(placeActionDTO);
        assertThat(actual, samePropertyValuesAs(action));
    }

    @Test
    void ReplyToActionTest() {
        when(actionService.updateActionWithReply(replyToActionDTO)).thenReturn(action);
        Action actual = appController.replyToAction(replyToActionDTO);
        assertThat(actual, samePropertyValuesAs(action));
    }

    @Test
    void MatchApproveTest() {
        when(actionService.updateActionWithApprove(matchApproveDTO)).thenReturn(action);
        Action actual = appController.matchApprove(matchApproveDTO);
        assertThat(actual, samePropertyValuesAs(action));
    }

    @Test
    void GetUserInfo() {
        UserInfoDTO dto = new UserInfoDTO(
                u1.getId(),
                u1.getName(),
                u1.getPhoneNumber(),
                u1.getEmail(),
                u1.getCity(),
                u1.getSharedAmount(),
                u1.getReceivedAmount(),
                u1.getFavouriteChoc());

        when(userService.getUserInfo(220L)).thenReturn(dto);
        assertThat(userController.getUserInfo(220L), samePropertyValuesAs(dto));
    }

}
