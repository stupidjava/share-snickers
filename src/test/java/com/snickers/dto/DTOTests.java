package com.snickers.dto;

import com.snickers.config.TestConfig;
import com.snickers.model.Action;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class DTOTests {

    @BeforeEach
    void TestSetup() {

    }

    @Test
    void MatchApproveSetActionIDTest() {
        MatchApproveDTO dto = new MatchApproveDTO();
        long id = 220L;
        dto.setActionId(id);
        assertEquals(id, dto.getActionId());
    }

    @Test
    void MatchApproveSetStatusTest() {
        MatchApproveDTO dto = new MatchApproveDTO();
        Action.Status status = Action.Status.OPEN;
        dto.setStatus(status);
        assertEquals(status, dto.getStatus());
    }

    @Test
    void PlaceActionSetTypeTest() {
        PlaceActionDTO dto = new PlaceActionDTO();
        Action.Type type = Action.Type.REQUEST;
        dto.setType(type);
        assertEquals(type, dto.getType());
    }

    @Test
    void PlaceActionSetInitiatorIDTest() {
        PlaceActionDTO dto = new PlaceActionDTO();
        long id = 220L;
        dto.setInitiatorId(id);
        assertEquals(id, dto.getInitiatorId());
    }

    @Test
    void PlaceActionSetChocTypeIDTest() {
        PlaceActionDTO dto = new PlaceActionDTO();
        long id = 220L;
        dto.setChocTypeId(id);
        assertEquals(id, dto.getChocTypeId());
    }

    @Test
    void PlaceActionSetChocWeightTest() {
        PlaceActionDTO dto = new PlaceActionDTO();
        int w = 10;
        dto.setChocWeight(w);
        assertEquals(w, dto.getChocWeight());
    }

    @Test
    void PlaceActionSetCommentTest() {
        PlaceActionDTO dto = new PlaceActionDTO();
        String comment = "comment";
        dto.setComment(comment);
        assertEquals(comment, dto.getComment());
    }

    @Test
    void ReplyToActionSetActionIDTest() {
        ReplyToActionDTO dto = new ReplyToActionDTO();
        int id = 220;
        dto.setActionId(id);
        assertEquals(id, dto.getActionId());
    }

    @Test
    void ReplyToActionSetStatusTest() {
        ReplyToActionDTO dto = new ReplyToActionDTO();
        Action.Status status = Action.Status.CANCELED;
        dto.setStatus(status);
        assertEquals(status, dto.getStatus());
    }

    @Test
    void ReplyToActionSetAcceptorIDTest() {
        ReplyToActionDTO dto = new ReplyToActionDTO();
        int id = 200;
        dto.setAcceptorId(id);
        assertEquals(id, dto.getAcceptorId());
    }

    @Test
    void UserInfoSetUserIDTest() {
        UserInfoDTO dto = new UserInfoDTO();
        long id = 220L;
        dto.setUserId(id);
        assertEquals(id, dto.getUserId());
    }

    @Test
    void UserInfoSetUsernameTest() {
        UserInfoDTO dto = new UserInfoDTO();
        String username = "username";
        dto.setUsername(username);
        assertEquals(username, dto.getUsername());
    }

    @Test
    void UserInfoSetPhoneNumberTest() {
        UserInfoDTO dto = new UserInfoDTO();
        String pnumber = "+7777";
        dto.setPhoneNumber(pnumber);
        assertEquals(pnumber, dto.getPhoneNumber());
    }

    @Test
    void UserInfoSetEmailTest() {
        UserInfoDTO dto = new UserInfoDTO();
        String email = "hhhh";
        dto.setEmail(email);
        assertEquals(email, dto.getEmail());
    }

    @Test
    void UserInfoSetCityTest() {
        UserInfoDTO dto = new UserInfoDTO();
        String city = "kzn";
        dto.setCity(city);
        assertEquals(city, dto.getCity());
    }

    @Test
    void UserInfoSetSharedTest() {
        UserInfoDTO dto = new UserInfoDTO();
        int shared = 220;
        dto.setSharedAmount(shared);
        assertEquals(shared, dto.getSharedAmount());
    }

    @Test
    void UserInfoSetReceivedTest() {
        UserInfoDTO dto = new UserInfoDTO();
        int rec = 220;
        dto.setReceivedAmount(rec);
        assertEquals(rec, dto.getReceivedAmount());
    }
}

