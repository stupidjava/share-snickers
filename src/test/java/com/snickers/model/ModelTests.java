package com.snickers.model;

import com.snickers.config.TestConfig;
import com.snickers.dto.MatchApproveDTO;
import com.snickers.dto.PlaceActionDTO;
import com.snickers.dto.ReplyToActionDTO;
import com.snickers.dto.UserInfoDTO;
import com.snickers.repository.ActionRepository;
import com.snickers.repository.ChocTypeRepository;
import com.snickers.repository.UserRepository;
import com.snickers.service.ActionService;
import com.snickers.service.ChocTypeService;
import com.snickers.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class ModelTests {

    @BeforeEach
    void TestSetup() {

    }

    @Test
    void ActionGetIDTest() {
        Action action = new Action();
        long id = 220L;
        action.setId(id);
        assertEquals(id, action.getId());
    }

    @Test
    void ActionGetTypeTest() {
        Action action = new Action();
        Action.Type type = Action.Type.OFFER_PERSONAL;
        action.setType(type);
        assertEquals(type, action.getType());
    }

    @Test
    void ActionGetStatusTest() {
        Action action = new Action();
        Action.Status status = Action.Status.CLOSED;
        action.setStatus(status);
        assertEquals(status, action.getStatus());
    }

    @Test
    void ActionGetInitiatorTest() {
        Action action = new Action();
        long id = 220L;
        action.setInitiator(id);
        assertEquals(id, action.getInitiator());
    }

    @Test
    void ActionGetAcceptorTest() {
        Action action = new Action();
        long id = 220L;
        action.setAcceptor(id);
        assertEquals(id, action.getAcceptor());
    }

    @Test
    void ActionGetChocTypeTest() {
        Action action = new Action();
        long type = 220L;
        action.setChocType(type);
        assertEquals(type, action.getChocType());
    }

    @Test
    void ChocGetIDTest() {
        ChocType chocType = new ChocType();
        long id = 220L;
        chocType.setId(id);
        assertEquals(id, chocType.getId());
    }

    @Test
    void ChocGetNameTest() {
        ChocType chocType = new ChocType();
        String name = "name";
        chocType.setName(name);
        assertEquals(name, chocType.getName());
    }

    @Test
    void ChocGetDescriptionTest() {
        ChocType chocType = new ChocType();
        String desc = "desc";
        chocType.setDescription(desc);
        assertEquals(desc, chocType.getDescription());
    }

    @Test
    void UserGetIDTest() {
        User user = new User();
        long id = 220L;
        user.setId(id);
        assertEquals(id, user.getId());
    }

    @Test
    void UserGetAdminTest() {
        User user = new User();
        boolean isAdmin = false;
        user.setAdmin(isAdmin);
        assertEquals(isAdmin, user.getAdmin());
    }

    @Test
    void UserGetNameTest() {
        User user = new User();
        String name = "name";
        user.setName(name);
        assertEquals(name, user.getName());
    }

    @Test
    void UserGetPhoneNumberTest() {
        User user = new User();
        String pnumber = "+777";
        user.setPhoneNumber(pnumber);
        assertEquals(pnumber, user.getPhoneNumber());
    }

    @Test
    void UserGetEmailTest() {
        User user = new User();
        String email = "hhhhh";
        user.setEmail(email);
        assertEquals(email, user.getEmail());
    }

    @Test
    void UserGetCityTest() {
        User user = new User();
        String city = "kzn";
        user.setCity(city);
        assertEquals(city, user.getCity());
    }

    @Test
    void UserGetSharedTest() {
        User user = new User();
        int shared = 220;
        user.setSharedAmount(shared);
        assertEquals(shared, user.getSharedAmount());
    }

    @Test
    void UserGetReceivedTest() {
        User user = new User();
        int rec = 220;
        user.setReceivedAmount(rec);
        assertEquals(rec, user.getReceivedAmount());
    }
}

