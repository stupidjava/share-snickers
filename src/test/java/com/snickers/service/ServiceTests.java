package com.snickers.service;

import com.snickers.config.TestConfig;
import com.snickers.dto.MatchApproveDTO;
import com.snickers.dto.PlaceActionDTO;
import com.snickers.dto.ReplyToActionDTO;
import com.snickers.dto.UserInfoDTO;
import com.snickers.model.Action;
import com.snickers.model.ChocType;
import com.snickers.model.User;
import com.snickers.repository.ActionRepository;
import com.snickers.repository.ChocTypeRepository;
import com.snickers.repository.CredentialsRepository;
import com.snickers.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class ServiceTests {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CredentialsRepository credentialsRepository;

    @Autowired
    private ActionRepository actionRepository;

    @Autowired
    private ChocTypeRepository chocTypeRepository;

    private User u1, u2;
    private UserInfoDTO uidto1, uidto2;
    private Action a1;
    private PlaceActionDTO padto1;
    private ReplyToActionDTO rtadto1;
    private MatchApproveDTO madto1;
    private ChocType chocType;

    @BeforeEach
    void TestSetup() {
        u1 = new User();
        u1.setId(220L);
        u1.setName("name_1");
        u1.setReceivedAmount(5);
        u1.setSharedAmount(1);
        u1.setFavouriteChoc(1);

        u2 = new User();
        u2.setId(221L);
        u2.setName("name_2");
        u2.setReceivedAmount(1);
        u2.setSharedAmount(5);
        u2.setFavouriteChoc(1);

        uidto1 = new UserInfoDTO(
                u1.getId(),
                u1.getName(),
                u1.getPhoneNumber(),
                u1.getEmail(),
                u1.getCity(),
                u1.getSharedAmount(),
                u1.getReceivedAmount(),
                u1.getFavouriteChoc()
        );

        uidto2 = new UserInfoDTO(
                u2.getId(),
                u2.getName(),
                u2.getPhoneNumber(),
                u2.getEmail(),
                u2.getCity(),
                u2.getSharedAmount(),
                u2.getReceivedAmount(),
                u2.getFavouriteChoc()
        );

        a1 = new Action();
        a1.setId(220L);
        a1.setType(Action.Type.OFFER_PERSONAL);
        a1.setInitiator(220L);
        a1.setAcceptor(221L);
        a1.setChocType(220L);
        a1.setChocWeight(10);
        a1.setComment("WOW");
        a1.setStatus(Action.Status.OPEN);

        padto1 = new PlaceActionDTO(
                a1.getType(),
                a1.getInitiator(),
                a1.getChocType(),
                a1.getChocWeight(),
                a1.getComment()
        );

        rtadto1 = new ReplyToActionDTO(
                a1.getId(),
                a1.getStatus(),
                a1.getAcceptor()
        );

        madto1 = new MatchApproveDTO(
                a1.getId(),
                a1.getStatus()
        );

        chocType = new ChocType();
        chocType.setId(220L);
        chocType.setName("choc_type");
        chocType.setDescription("description");
    }

    @Test
    void GetUserRatingTest() {

        when(userRepository.findAll()).thenReturn(
                new ArrayList<>(){
                    {
                        add(u1);
                        add(u2);
                    }
                }
        );

        UserService userService = new UserService(userRepository, credentialsRepository);

        ArrayList<UserInfoDTO> exp_shared = new ArrayList<>() {{ add(uidto2); add(uidto1); }};
        ArrayList<UserInfoDTO> exp_receiv = new ArrayList<>() {{ add(uidto1); add(uidto2); }};

        ArrayList<UserInfoDTO> act_shared = (ArrayList<UserInfoDTO>) userService.getUserRating(true);
        ArrayList<UserInfoDTO> act_receiv = (ArrayList<UserInfoDTO>) userService.getUserRating(false);

        assertEquals(exp_shared.size(), act_shared.size());
        assertEquals(exp_receiv.size(), act_receiv.size());

        assertThat(act_shared.get(0), samePropertyValuesAs(exp_shared.get(0)));
        assertThat(act_shared.get(1), samePropertyValuesAs(exp_shared.get(1)));

        assertThat(act_receiv.get(0), samePropertyValuesAs(exp_receiv.get(0)));
        assertThat(act_receiv.get(1), samePropertyValuesAs(exp_receiv.get(1)));
    }

    @Test
    void GetUserInfoTest() {
        long uid = u1.getId();
        when(userRepository.findById(uid)).thenReturn(Optional.ofNullable(u1));
        UserService userService = new UserService(userRepository, credentialsRepository);
        assertThat(userService.getUserInfo(uid), samePropertyValuesAs(uidto1));
    }

    @Test
    void CreateUserTest() {
        when(userRepository.save(u1)).thenReturn(u1);
        UserService userService = new UserService(userRepository, credentialsRepository);
        User actual = userService.createUser(u1);
        assertThat(actual, samePropertyValuesAs(u1));
    }

    @Test
    void UpdateUserTest() {
        when(userRepository.save(u1)).thenReturn(u1);
        UserService userService = new UserService(userRepository, credentialsRepository);
        User actual = userService.updateUser(u1);
        assertThat(actual, samePropertyValuesAs(u1));
    }

    @Test
    void GetUserByIDTest() {
        when(userRepository.findById(u1.getId())).thenReturn(Optional.ofNullable(u1));
        UserService userService = new UserService(userRepository, credentialsRepository);
        User actual = userService.getUserById(u1.getId());
        assertThat(actual, samePropertyValuesAs(u1));
    }

    @Test
    void DeleteUserTest() {
        UserService userService = new UserService(userRepository, credentialsRepository);
        userService.deleteUser(u1);
        verify(userRepository).delete(u1);
    }

    @Test
    void CreateActionTest() {
        when(actionRepository.save(a1)).thenReturn(a1);
        ActionService actionService = new ActionService(actionRepository, userRepository);
        Action actual = actionService.createAction(a1);
        assertThat(actual, samePropertyValuesAs(a1));
    }

    @Test
    void UpdateActionTest() {
        when(actionRepository.save(a1)).thenReturn(a1);
        ActionService actionService = new ActionService(actionRepository, userRepository);
        Action actual = actionService.updateAction(a1);
        assertThat(actual, samePropertyValuesAs(a1));
    }

    @Test
    void GetActionByIDTest() {
        when(actionRepository.findById(a1.getId())).thenReturn(Optional.ofNullable(a1));
        ActionService actionService = new ActionService(actionRepository, userRepository);
        Action actual = actionService.getActionById(a1.getId());
        assertThat(actual, samePropertyValuesAs(a1));
    }

    @Test
    void CreatePlaceActionTest() {
        a1.setId(null);
        a1.setAcceptor(null);
        when(actionRepository.save(a1)).thenReturn(a1);
        ActionService actionService = new ActionService(actionRepository, userRepository);
        Action actual = actionService.createPlaceAction(padto1);
        assertThat(actual, samePropertyValuesAs(a1));
    }

    @Test
    void UpdateActionWithReply() {
        when(actionRepository.findById(a1.getId())).thenReturn(Optional.ofNullable(a1));
        when(actionRepository.save(a1)).thenReturn(a1);
        ActionService actionService = new ActionService(actionRepository, userRepository);
        Action actual = actionService.updateActionWithReply(rtadto1);
        assertThat(actual, samePropertyValuesAs(a1));
        assertEquals(rtadto1.getStatus(), actual.getStatus());
        assertEquals(rtadto1.getAcceptorId(), actual.getAcceptor());
    }

    @Test
    void UpdateActionWithApprove() {
        int oldShare = u1.getSharedAmount();
        int oldReceive = u2.getReceivedAmount();

        when(actionRepository.findById(a1.getId())).thenReturn(Optional.ofNullable(a1));
        when(actionRepository.save(a1)).thenReturn(a1);

        when(userRepository.findById(a1.getInitiator())).thenReturn(Optional.ofNullable(u1));
        when(userRepository.findById(a1.getAcceptor())).thenReturn(Optional.ofNullable(u2));

        when(userRepository.save(u1)).thenReturn(null);
        when(userRepository.save(u2)).thenReturn(null);

        ActionService actionService = new ActionService(actionRepository, userRepository);
        Action actual = actionService.updateActionWithApprove(madto1);

        assertThat(actual, samePropertyValuesAs(a1));
        assertEquals(madto1.getStatus(), actual.getStatus());
        assertEquals(oldShare + 1, u1.getSharedAmount());
        assertEquals(oldReceive + 1, u2.getReceivedAmount());
    }

    @Test
    void CreateChocTypeTest() {
        when(chocTypeRepository.save(chocType)).thenReturn(chocType);
        ChocTypeService chocTypeService = new ChocTypeService(chocTypeRepository);
        ChocType actual = chocTypeService.createChocType(chocType);
        assertThat(actual, samePropertyValuesAs(chocType));
    }

    @Test
    void GetChocTypeByIdTest() {
        when(chocTypeRepository.findById(chocType.getId())).thenReturn(Optional.ofNullable(chocType));
        ChocTypeService chocTypeService = new ChocTypeService(chocTypeRepository);
        ChocType actual = chocTypeService.getChocTypeById(chocType.getId());
        assertThat(actual, samePropertyValuesAs(chocType));
    }

    @Test
    void ChocUpdateActionTest() {
        when(chocTypeRepository.save(chocType)).thenReturn(chocType);
        ChocTypeService chocTypeService = new ChocTypeService(chocTypeRepository);
        ChocType actual = chocTypeService.updateAction(chocType);
        assertThat(actual, samePropertyValuesAs(chocType));
    }

}

