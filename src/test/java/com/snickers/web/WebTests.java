package com.snickers.web;

import com.snickers.config.TestConfig;
import com.snickers.dto.*;
import com.snickers.model.Action;
import com.snickers.model.User;
import com.snickers.service.ActionService;
import com.snickers.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.ui.Model;

import java.security.Principal;
import java.util.ArrayList;
import java.util.LinkedList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class WebTests {

    @Autowired
    private UserRatingViewController userRatingViewController;

    @Autowired
    private UserInfoViewController userInfoViewController;

    @Autowired
    private IndexViewController indexViewController;

    @Autowired
    private UserService userService;

    @Autowired
    private ActionService actionService;

    private User u1;
    private User u2;

    @BeforeEach
    void TestSetup() {
        u1 = new User();
        u1.setId(220L);
        u1.setName("name_1");
        u1.setReceivedAmount(5);
        u1.setSharedAmount(1);
        u1.setFavouriteChoc(1);

        u2 = new User();
        u2.setId(221L);
        u2.setName("name_2");
        u2.setReceivedAmount(1);
        u2.setSharedAmount(5);
        u2.setFavouriteChoc(1);
    }

    @Test
    void UserRatingLeaderboards() {

        UserInfoDTO u1 = mock(UserInfoDTO.class);
        ArrayList<UserInfoDTO> shared = new ArrayList<>();
        shared.add(u1);

        UserInfoDTO u2 = mock(UserInfoDTO.class);
        ArrayList<UserInfoDTO> receiv = new ArrayList<>();
        receiv.add(u2);

        when(userService.getUserRating(true)).thenReturn(shared);
        when(userService.getUserRating(false)).thenReturn(receiv);

        ArrayList<Pair<Integer, UserInfoDTO>> sharedRating = new ArrayList<>();
        ArrayList<Pair<Integer, UserInfoDTO>> receivRating = new ArrayList<>();

        Model model = mock(Model.class);
        when(model.addAttribute("shared_users", sharedRating)).thenReturn(null);
        when(model.addAttribute("received_users", receivRating)).thenReturn(null);

        assertEquals("leaderboards", userRatingViewController.leaderboards(model));
    }

    @Test
    void GetIndex() {
        Principal principal = mock(Principal.class);
        when(userService.getUserByUsername(principal.getName())).thenReturn(u1);
        assertEquals("index", indexViewController.getIndex(mock(Model.class), principal));
    }

    @Test
    void GetHome() {
        assertEquals("redirect:/", indexViewController.getHome(mock(Model.class)));
    }

    @Test
    void GetActiveActionsTest() {
        Principal principal = mock(Principal.class);

        when(userService.getUserByUsername(principal.getName())).thenReturn(u2);
        when(userService.getUserById(220L)).thenReturn(u1);
        when(userService.getUserById(221L)).thenReturn(u2);

        Action a1 = new Action();
        a1.setId(330L);
        a1.setInitiator(220L);
        a1.setAcceptor(null);
        a1.setType(Action.Type.REQUEST);

        Action a2 = new Action();
        a2.setId(331L);
        a2.setInitiator(221L);
        a2.setAcceptor(220L);
        a2.setType(Action.Type.OFFER_PUBLIC);

        Action a3 = new Action();
        a3.setId(332L);
        a3.setInitiator(220L);
        a3.setAcceptor(221L);
        a3.setType(Action.Type.OFFER_PUBLIC);

        ArrayList<Action> openActions = new ArrayList<>();
        openActions.add(a1);
        openActions.add(a2);
        openActions.add(a3);
        when(actionService.getUserOpenActions(u2.getId())).thenReturn(openActions);

        ActiveActionDTO aadto1 = new ActiveActionDTO();
        aadto1.setReceiver(false);
        aadto1.setActionID(330L);
        aadto1.setHasAcceptor(false);
        aadto1.setComment("Waiting for acceptor");
        aadto1.setOtherName("Guy without snickers");

        ActiveActionDTO aadto2 = new ActiveActionDTO();
        aadto2.setReceiver(false);
        aadto2.setActionID(331L);
        aadto2.setHasAcceptor(true);
        aadto2.setOtherName(u1.getName());
        aadto2.setOtherLocation(u1.getCity());
        aadto2.setOtherPhone(u1.getPhoneNumber());
        aadto2.setComment("Share the item and wait for approve");

        ActiveActionDTO aadto3 = new ActiveActionDTO();
        aadto3.setReceiver(true);
        aadto3.setActionID(332L);
        aadto3.setHasAcceptor(true);
        aadto3.setOtherName(u1.getName());
        aadto3.setOtherLocation(u1.getCity());
        aadto3.setOtherPhone(u1.getPhoneNumber());
        aadto3.setComment("Receive the item and approve on the web");


        ArrayList<ActiveActionDTO> expected = new ArrayList<>();
        expected.add(aadto1);
        expected.add(aadto2);
        expected.add(aadto3);

        ArrayList<ActiveActionDTO> actual = (ArrayList<ActiveActionDTO>) indexViewController.getActiveActions(u2.getId());
        assertEquals(expected.size(), actual.size());
        for (int i = 0; i < expected.size(); i++) {
            assertThat(actual.get(i), samePropertyValuesAs(expected.get(i)));
        }
    }

    @Test
    void GetAvailableActionsTest() {
        when(userService.getUserById(221L)).thenReturn(u2);

        Action a1 = new Action();
        a1.setId(330L);
        a1.setInitiator(221L);
        a1.setAcceptor(null);
        a1.setType(Action.Type.REQUEST);

        Action a2 = new Action();
        a2.setId(331L);
        a2.setInitiator(221L);
        a2.setAcceptor(220L);
        a2.setType(Action.Type.OFFER_PUBLIC);

        ArrayList<Action> sharedActions = new ArrayList<>();
        ArrayList<Action> receiveActions = new ArrayList<>();

        sharedActions.add(a2);
        receiveActions.add(a1);

        when(actionService.getAllShareActions()).thenReturn(sharedActions);
        when(actionService.getAllReceiveActions()).thenReturn(receiveActions);

        AvailableActionDTO dto1 = new AvailableActionDTO();
        dto1.setInitiatorName(u2.getName());
        dto1.setInitiatorPhone(u2.getPhoneNumber());
        dto1.setInitiatorLocation(u2.getCity());
        dto1.setActionID(a1.getId());
        dto1.setComment(a1.getComment());
        dto1.setChocTypeId(a1.getChocType());
        dto1.setType(a1.getType());
        dto1.setInitiatorId(a1.getInitiator());

        AvailableActionDTO dto2 = new AvailableActionDTO();
        dto2.setInitiatorName(u2.getName());
        dto2.setInitiatorPhone(u2.getPhoneNumber());
        dto2.setInitiatorLocation(u2.getCity());
        dto2.setActionID(a2.getId());
        dto2.setComment(a2.getComment());
        dto2.setChocTypeId(a2.getChocType());
        dto2.setType(a2.getType());
        dto2.setInitiatorId(a2.getInitiator());

        LinkedList<AvailableActionDTO> expected1 = new LinkedList<>();
        expected1.add(dto1);

        LinkedList<AvailableActionDTO> expected2 = new LinkedList<>();
        expected2.add(dto2);

        LinkedList<AvailableActionDTO> actual1 = (LinkedList<AvailableActionDTO>)
                indexViewController.getAvailableActions(false, u1.getId());

        assertEquals(expected1.size(), actual1.size());
        for (int i = 0; i < expected1.size(); i++) {
            assertThat(actual1.get(i), samePropertyValuesAs(expected1.get(i)));
        }

        LinkedList<AvailableActionDTO> actual2 = (LinkedList<AvailableActionDTO>)
                indexViewController.getAvailableActions(true, u1.getId());

        assertEquals(expected2.size(), actual2.size());
        for (int i = 0; i < expected2.size(); i++) {
            assertThat(actual2.get(i), samePropertyValuesAs(expected2.get(i)));
        }

    }

    @Test
    void NewActionTest() {
        Principal principal = mock(Principal.class);

        when(userService.getUserByUsername(principal.getName())).thenReturn(u1);

        PlaceActionDTO placeActionDTO = new PlaceActionDTO();
        placeActionDTO.setType(Action.Type.REQUEST);

        placeActionDTO.setChocTypeId(0L);
        placeActionDTO.setComment("comment");
        placeActionDTO.setInitiatorId(u1.getId());
        placeActionDTO.setChocWeight(0);

        assertEquals("redirect:/", indexViewController.newAction(0, "comment", principal));
        ArgumentCaptor<PlaceActionDTO> argument = ArgumentCaptor.forClass(PlaceActionDTO.class);
        verify(actionService, atLeast(0)).createPlaceAction(argument.capture());
        assertThat(argument.getValue(), samePropertyValuesAs(placeActionDTO));
    }

    @Test
    void AcceptActionTest() {
        Principal principal = mock(Principal.class);

        when(userService.getUserByUsername(principal.getName())).thenReturn(u1);

        ReplyToActionDTO replyToActionDTO = new ReplyToActionDTO(0, Action.Status.OPEN, u1.getId());

        assertEquals("redirect:/", indexViewController.acceptAction(0, principal));
        ArgumentCaptor<ReplyToActionDTO> argument = ArgumentCaptor.forClass(ReplyToActionDTO.class);
        verify(actionService, atLeast(0)).updateActionWithReply(argument.capture());
        assertThat(argument.getValue(), samePropertyValuesAs(replyToActionDTO));
    }

    @Test
    void CancelActionTest() {
        Principal principal = mock(Principal.class);

        when(userService.getUserByUsername(principal.getName())).thenReturn(u1);

        ReplyToActionDTO replyToActionDTO = new ReplyToActionDTO(0, Action.Status.CANCELED, u1.getId());

        assertEquals("redirect:/", indexViewController.cancelAction(0, principal));
        ArgumentCaptor<ReplyToActionDTO> argument = ArgumentCaptor.forClass(ReplyToActionDTO.class);
        verify(actionService, atLeast(0)).updateActionWithReply(argument.capture());
        assertThat(argument.getValue(), samePropertyValuesAs(replyToActionDTO));
    }

    @Test
    void ApproveActionTest() {
        MatchApproveDTO matchApproveActionDTO = new MatchApproveDTO(0, Action.Status.CLOSED);

        assertEquals("redirect:/", indexViewController.approveAction(0));
        ArgumentCaptor<MatchApproveDTO> argument = ArgumentCaptor.forClass(MatchApproveDTO.class);
        verify(actionService, atLeast(0)).updateActionWithApprove(argument.capture());
        assertThat(argument.getValue(), samePropertyValuesAs(matchApproveActionDTO));
    }

    @Test
    void UpdateDataTest() {
        Principal principal = mock(Principal.class);
        when(userService.getUserByUsername(principal.getName())).thenReturn(u1);
        when(userService.updateUser(u1)).thenReturn(u1);
        User exp = new User();
        exp.setId(220L);
        exp.setName("name");
        exp.setCity("city");
        exp.setPhoneNumber("123");
        exp.setReceivedAmount(5);
        exp.setSharedAmount(1);
        exp.setFavouriteChoc(1);
        assertEquals("redirect:/user", userInfoViewController.updateData("name", "city", "123", principal));
        assertThat(u1, samePropertyValuesAs(exp));
    }
}
